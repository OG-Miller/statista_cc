import React from 'react';
import './listItem.css';

const ListItem = props => {
	return (
		<>
			{(props.option === 'Fizz' && <li className='listItem__fizz'>{props.option}</li>) ||
				(props.option === 'Buzz' && <li className='listItem__buzz'>{props.option}</li>) ||
				(props.option === 'FizzBuzz' && <li className='listItem__fizzBuzz'>{props.option}</li>) ||
				(props.option !== ('Fizz' || 'Buzz' || 'FizzBuzz') && (
					<li className='listItem__number'>{props.option}</li>
				))}
		</>
	);
};

export default ListItem;
