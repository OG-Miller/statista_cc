import React from 'react';
import logo from './logo.svg';
import './App.css';
import './ListItem';
import ListItem from './ListItem';

function App() {
	const fizzBuzzlist = [];

	const fizzBuzzChooser = () => {
		for (let i = 1; i < 101; i++) {
			if (i % 3 === 0 && i % 5 === 0) {
				fizzBuzzlist.push('FizzBuzz');
			} else if (i % 3 === 0) {
				fizzBuzzlist.push('Fizz');
			} else if (i % 5 === 0) {
				fizzBuzzlist.push('Buzz');
			} else {
				fizzBuzzlist.push(i);
			}
		}
	};

	fizzBuzzChooser();

	//map list item components
	return (
		<div className='App'>
			<div className='horizontalDivider'></div>
			<h1 className='fizzBuzz__title'>FIZZBUZZ LIST</h1>
			<div className='fizzBuzzList'>
				<ul>
					{fizzBuzzlist.map(item => (
						<ListItem option={item} />
					))}
				</ul>
			</div>
		</div>
	);
}

export default App;
